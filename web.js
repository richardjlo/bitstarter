var express = require('express');
var fs = require('fs');
var express = require("express");  


var app = express.createServer(express.logger());

app.get('/', function(request, response) {
	var buf = new Buffer(fs.readFileSync('index.html'));
	var message = buf.toString();
	response.send(message);	
});

var port = process.env.PORT || 8080;
app.listen(port, function() {
  console.log("Listening on " + port);
});